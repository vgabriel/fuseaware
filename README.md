## fuseAware

Hello,

Using Laravel for this taks as it's my main framework.

I've used a version group for routes because this API might change and it is easier to make a new one without affecting the user.

Resources are used to get the data for the API and factories to generate dummy data.

Test and Migrations are also included so please feel free to `php artisan migrate` and `php artisan db:seed`

Don't hesitate to get in touch if you have any questions or concerns.