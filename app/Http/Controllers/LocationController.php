<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\User;

use App\Http\Resources\Location as LocationResource;
use App\Location;

class LocationController extends Controller
{

    /*
        SHOW
    */
    public function show($id) : LocationResource
    {
        return new LocationResource(Location::find($id));
    }

    /*
        UPDATE
    */
    public function update(Request $request, Location $location)
    {
        $location->update($request->all());

        return response()->json($location, 200);
    }

    /*
        STORE
    */
    public function store(Request $request)
    {
        $location = Location::create($request->all());

        return response()->json($location, 201);
    }

    /*
        DELETE
    */
    public function delete(Location $location)
    {
        $location->delete();

        return response()->json(null, 204);
    }
}
