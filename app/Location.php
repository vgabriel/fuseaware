<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Location extends Model
{
    protected $fillable = ['latitude', 'longitude', 'user_id'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
