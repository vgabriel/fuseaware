<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create 5 locations for each user
        factory(App\User::class, 5)->create()->each(function ($user) {
            $user->locations()->saveMany(factory(App\Location::class, 5)->make());
        });
    }
}
