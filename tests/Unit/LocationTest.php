<?php
namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Location;


class LocationTest extends TestCase
{
    use RefreshDatabase;

    public function testsLocationIsUpdatedCorrectly()
        {
            $location = factory(Location::class, 1)->create(['user_id' => '1']);

            $payload = [
                'latitude' => '-65.184422',
                'longitude' => '49.468524',
                'user_id' => '1'
            ];

            $response = $this->json('PUT', '/api/v1/location/' . $location->first()->id, $payload)
            ->assertStatus(200)
            ->assertJson([ 
                'id' => 1, 
                'latitude' => '-65.184422',
                'longitude' => '49.468524',
                'user_id' => '1'
            ]);
    }

    public function testsLocationIsDeletedCorrectly()
    {
        $location = factory(Location::class, 1)->create(['user_id' => '1']);

        $this->json('DELETE', '/api/v1/location/' . $location->first()->id)
            ->assertStatus(204);
    }

    public function testsLocationIsCreatedCorrectly()
    {
        $location = factory(Location::class, 1)->create(['user_id' => '1']);

        $payload = [
            'latitude' => '-65.184422',
            'longitude' => '49.468524',
            'user_id' => '1'
        ];

        $this->json('POST', '/api/v1/location', $payload)
            ->assertStatus(201)
            ->assertJson([
                'latitude' => '-65.184422',
                'longitude' => '49.468524',
                'user_id' => '1'
                ]);
    }

}